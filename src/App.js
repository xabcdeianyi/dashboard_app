import './App.css';
import React, {useState} from 'react';
import Row from './widget/row';
import moment from "moment";
import leftAr from "./assets/arrow-left.png";
import calendar from "./assets/calendar.png";
import rightAr from "./assets/arrow-right.png";
import styled from "styled-components";
import {IconButton, Paper, TextField, Tooltip} from "@material-ui/core";
import axios from "axios";
import face1 from "./assets/level1.png";
import face2 from "./assets/level2.png";
import face3 from "./assets/level3.png";
import face4 from "./assets/level4.png";
import BtnRound from "./widget/btn_round";

let init = 0;
const url = window.location.href;
let arr = url.split("/");
let location_url = arr[0] + "//" + arr[2]+"/dev/wellbeing";
// let location_url = "http://localhost:8080/dev/wellbeing";
function App() {


    let [date = moment(new Date()).add(-1, 'days').toDate(), setDate] = useState();
    let [data = [], setData] = useState();

    if (init < 2) {
        getAPI();
        init++;
    }

    function dateChange(dir) {
        if (dir == 0) {
            setDate(date = moment(date).add(-1, 'days').toDate());
        } else {
            setDate(date = moment(date).add(1, 'days').toDate());
        }
    }

    async function getAPI() {
        let params = {
            headers: {
                'Content-Type': 'application/json',
            },
            response: false,
            params: {
                date: moment(date).format('YYYY/MM/DD'),
            },
        }
        await axios.get(location_url, params)
            .then(res => {
                setData(data = res.data);
            })
        console.log("myData", data);
    }

    const dayStatus = (status, vs, sleep, IADL, UI, rawVS, rawWP, rawIADL, IADLlevels, WPlevels, VSlevels) => {

        const displayColor = (num) => {
            if (num === -1) {
                return '#dddddd'
            } else if (num === 0) {
                return '#333333'
            } else if (num === 1) {
                return '#58a878'
            } else if (num === 2) {
                return '#5b9bd5'
            } else if (num === 3) {
                return '#d97172'
            } else if (num === 4) {
                return '#632039'
            } else {
                return '#FFFFFF'
            }
        }

        const displayTTColor = (num) => {
            if (num === 0) {
                return '#ffffff'
            } else if (num === 1) {
                return '#88d8a8'
            } else if (num === 2) {
                return '#8bcbff'
            } else if (num === 3) {
                return '#ffa1a2'
            } else if (num === 4) {
                return '#f33039'
            } else {
                return '#FFFFFF'
            }
        }

        const displayText = (num) => {
            if (num === -1) {
                return 'Not Yet'
            } else if (num === 0) {
                return 'Bed Off'
            } else if (num === 1) {
                return 'Great'
            } else if (num === 2) {
                return 'Normal'
            } else if (num === 3) {
                return 'Attention'
            } else if (num === 4) {
                return 'Abnormal'
            } else {
                return ''
            }
        }

        const BtnColor = (colorCode) => {

            if (colorCode === -1) {
                return '#dddddd'
            } else if (colorCode === 0) {
                return '#777777'
            } else if (colorCode === 1) {
                return '#58a878'
            } else if (colorCode === 2) {
                return '#5b9bd5'
            } else if (colorCode === 3) {
                return '#d97172'
            } else if (colorCode === 4) {
                return '#632039'
            } else {
                return '#dddddd'
            }
        }

        const FaceIc = (faceCode) => {

            if (faceCode === 1) {
                return face1
            } else if (faceCode === 2) {
                return face2
            } else if (faceCode === 3) {
                return face3
            } else if (faceCode === 4) {
                return face4
            } else {
                return null
            }
        }

        return (
            <>
                <DashboardItem style={{justifyContent: 'center'}}>
                    <BtnRound
                        width={`120px`}
                        btncolor={displayColor(status)}>
                        <div style={{color: '#ffffff'}} className="Padding">{displayText(status)}</div>
                    </BtnRound>
                </DashboardItem>
                <DashboardItem style={{justifyContent: 'center'}}>
                    <span>
                        <img className="UserIc" src={FaceIc(UI)}/>
                    </span>
                </DashboardItem>
                <DashboardItem style={{justifyContent: 'center'}}>
                    <Tooltip title={
                        <h2>
                            <div style={{color: displayTTColor(VSlevels.bloodGlucose)}}>Blood
                                Glucose:&nbsp;&nbsp;{rawVS.bloodGlucose} &nbsp;&nbsp; mg/dl<br></br><br></br></div>
                            <div style={{color: displayTTColor(VSlevels.bloodPressure_sys)}}>Blood Pressure
                                Systole:&nbsp;&nbsp;{rawVS.bloodPressure_sys} &nbsp;&nbsp;mm/Hg<br></br><br></br>
                            </div>
                        </h2>}>
                            <span>
                                <BtnRound
                                    className="BtnText"
                                    width={`50px`}
                                    btncolor={BtnColor(vs)}>
                                <div style={{color: '#ffffff'}}>VS</div>
                                </BtnRound>
                            </span>
                    </Tooltip>
                </DashboardItem>
                <DashboardItem style={{justifyContent: 'center'}}>
                    <div className="MarginSide">
                        <Tooltip title={
                            <h2>
                                <div style={{color: displayTTColor(WPlevels.inbedtimeHour)}}>Time in
                                    bed:&nbsp;&nbsp;{rawWP.inbedtimeHour}&nbsp;&nbsp;hrs<br></br><br></br></div>
                                <div style={{color: displayTTColor(WPlevels.pressuretimeHour)}}>Pressure
                                    Time:&nbsp;&nbsp;{rawWP.pressuretimeHour}&nbsp;&nbsp;hrs<br></br><br></br></div>
                                <div style={{color: displayTTColor(WPlevels.sleeptimeHour)}}>Sleep
                                    Time:&nbsp;&nbsp;{rawWP.sleeptimeHour} &nbsp;&nbsp;hrs<br></br><br></br></div>
                                <div style={{color: displayTTColor(WPlevels.sleepEfficiency)}}>Sleep
                                    Efficiency:&nbsp;&nbsp;{rawWP.sleepEfficiency} <br></br><br></br></div>
                                <div style={{color: displayTTColor(WPlevels.correlation)}}>On/Off Bed Pattern
                                    Correlation:&nbsp;&nbsp;{rawWP.correlation} <br></br><br></br></div>
                            </h2>}>
                        <span>
                            <BtnRound
                                className="BtnText"
                                width={`50px`}
                                btncolor={BtnColor(sleep)}>
                                <div style={{color: '#ffffff'}}>SS</div>
                            </BtnRound>
                        </span>
                        </Tooltip>
                    </div>
                </DashboardItem>
                <DashboardItem style={{justifyContent: 'center'}}>
                    <div className="MarginSide">
                        <Tooltip title={
                            <h2>
                                <div style={{color: displayTTColor(IADLlevels.activeScore_cook)}}>Cook
                                    Score:&nbsp;&nbsp;{rawIADL.activeScore_cook} <br></br><br></br></div>
                                <div style={{color: displayTTColor(IADLlevels.activeScore_hygiene)}}>Hygiene
                                    Score:&nbsp;&nbsp;{rawIADL.activeScore_hygiene} <br></br><br></br></div>
                                <div style={{color: displayTTColor(IADLlevels.activeScore_leisure)}}>Leisure
                                    Score:&nbsp;&nbsp;{rawIADL.activeScore_liesure} <br></br><br></br></div>
                            </h2>}>
                        <span>
                            <BtnRound
                                className="BtnText"
                                width={`50px`}
                                btncolor={BtnColor(IADL)}>
                            <div style={{color: '#ffffff'}}>IADL</div>
                        </BtnRound>
                        </span>
                        </Tooltip>
                    </div>
                </DashboardItem>
            </>
        );
    }

    const idName = (id) => {
        if (id === "HOME001") {
            return "Kate";
        } else if (id === "HOME002") {
            return "John"
        } else if (id === "HOME003") {
            return "Anson"
        } else {
            return "Michael"
        }

    }

    return (
        <>
            <div className="AppBar">
                <Row>
                    <h3 className="AppBarTitle">Health and WellBeing Dashboard</h3>
                    <h3 className="AppBarSet"></h3>
                    <div className="AppBarSet">MANAGEMENT</div>
                    <span className="AppBarSet">SETTING</span>
                    {/*<span className="AppBarSet"><a className="AppBarSet" href="https://designer.mech.yzu.edu.tw/chinese">Language</a></span>*/}
                </Row>
            </div>
            <div className="Content">
                <DatePickerWrapper>
                    <SvgIconButton onClick={() => {
                        dateChange(0);
                        setData(data = []);
                        getAPI();
                    }}>
                        <img className="Icon" src={leftAr}/>
                    </SvgIconButton>
                    <span className="TextDesc" style={{marginLeft: `5px`, marginRight: `5px`}}>
                    <span className="TextNum">{date.getFullYear()}</span>&nbsp;{'/'}&nbsp;
                        <span className="TextNum">{date.getMonth() + 1}</span>&nbsp;{'/'}&nbsp;
                        <span className="TextNum">{date.getDate()}</span>&nbsp;{''}
                    </span>
                    <SvgIconButton style={{marginRight: `10px`}} onClick={() => {
                    }}>
                        <img className="Icon" src={calendar}/>
                    </SvgIconButton>
                    <SvgIconButton onClick={() => {
                        dateChange(1);
                        setData(data = []);
                        getAPI();
                    }}>
                        <img className="Icon" src={rightAr}/>
                    </SvgIconButton>
                </DatePickerWrapper>
                <DashboardTitle>
                    <div>Name</div>
                    <div>Status</div>
                    <div>User Input</div>
                    <div>Vital Sign</div>
                    <div>Sleep Status</div>
                    <div>IADL</div>
                </DashboardTitle>
                {/*<DashBoardPage/>*/}
                {data.map((item, index) => {
                    let {
                        Id,
                        Status,
                        VitalSign,
                        WhizPad,
                        IADL,
                        UserInput,
                        rawVS,
                        rawWP,
                        rawIADL,
                        IADLlevels,
                        WPlevels,
                        VSlevels
                    } = item;
                    return (
                        <DashboardCard style={{justifyContent: 'center'}}>
                            <Row height={'100%'} width={'100%'}>
                                <DashboardName>
                                    <div>{idName(Id)}</div>
                                </DashboardName>
                                {dayStatus(Status, VitalSign, WhizPad, IADL, UserInput, rawVS, rawWP, rawIADL, IADLlevels, WPlevels, VSlevels)}
                            </Row>
                        </DashboardCard>
                    )
                })}
            </div>
        </>
    );
}

const DatePickerWrapper = styled.div`
  width: 100%;
  height: 50px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const SvgIconButton = styled(IconButton)`
  && {
    align-self: center;
    padding: 3px;
  }

  &:focus {
    outline: none;
  }
`;

const DashboardCard = styled(Paper)`
  width: 100%;
  height: 80px;
  margin-bottom: 20px;
  cursor: pointer;

  &&& {
    border-radius: 8px;
  }
`;

const DashboardTitle = styled.div`
  width: 100%;
  height: 60px;
  display: flex;
  flex-direction: row;
  //justify-content: center;
  text-align: center;
  align-items: center;
  margin-top: 10px;
  margin-bottom: 10px;

  div {
    flex: 1;
    font-weight: bold;
    font-size: 1.1rem;
  }
`;

const DashboardName = styled.div`
  flex: 1;
  display: flex;
  text-align: center;
  flex-direction: column;
  width: 100%;
  height: 100%;
  justify-content: center;

  * {
    //padding-left: 30px;
  }

  div {
    font-weight: bold;
    font-size: 1.1rem;
  }

  p {
    margin: 2px 0 0 0;
    font-size: 0.95rem;
    color: #222222;
  }

  @media screen and (max-width: 768px) {
    * {
      padding-left: 10px;
    }
  }
`;

const DashboardItem = styled.div`
  flex: 1;
  width: auto;
  height: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  font-weight: bold;
  font-size: 1.1rem;

  span {
    font-weight: normal;
    font-size: 0.95rem;
    margin: 0 5px 0 5px;
  }

`;

export default App;
