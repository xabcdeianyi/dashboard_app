import React from 'react';
import axios from 'axios';
import Row from '../widget/row';
import styled from "styled-components";
import {ButtonBase, IconButton, Paper, Tooltip} from "@material-ui/core";
import face1 from "../assets/level1.png";
import face2 from "../assets/level2.png";
import face3 from "../assets/level3.png";
import face4 from "../assets/level4.png";
import BtnRound from "../widget/btn_round";

export default class DashBoardPage extends React.Component {

    state = {
        dashData: []
    }

    componentDidMount() {
        let params = {
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
                "Access-Control-Allow-Origin": "true"
            },
            response: false
        }
        axios.post(`https://6fq7qictw3.execute-api.us-west-2.amazonaws.com/haal/haal`, params)
            .then(res => {
                const dashData = res.data;
                this.setState({dashData});
            })
    }

    render() {

        const dayStatus = (status, vs, sleep, IADL, UI) => {

            const displayColor = (num) => {
                if (num === -1) {
                    return '#dddddd'
                } else if (num === 0) {
                    return '#777777'
                } else if (num === 1) {
                    return '#58a878'
                } else if (num === 2) {
                    return '#5b9bd5'
                } else if (num === 3) {
                    return '#d97172'
                } else if (num === 4) {
                    return '#632039'
                } else {
                    return '#FFFFFF'
                }
            }

            const displayText = (num) => {
                if (num === -1) {
                    return 'Not Yet'
                } else if (num === 0) {
                    return 'Bed Off'
                } else if (num === 1) {
                    return 'Great'
                } else if (num === 2) {
                    return 'Normal'
                } else if (num === 3) {
                    return 'Attention'
                } else if (num === 4) {
                    return 'Abnormal'
                } else {
                    return ''
                }
            }

            const BtnColor = (colorCode) => {

                if (colorCode === -1) {
                    return '#dddddd'
                } else if (colorCode === 0) {
                    return '#777777'
                } else if (colorCode === 1) {
                    return '#58a878'
                } else if (colorCode === 2) {
                    return '#5b9bd5'
                } else if (colorCode === 3) {
                    return '#d97172'
                } else if (colorCode === 4) {
                    return '#632039'
                } else {
                    return '#dddddd'
                }
            }

            const FaceIc = (faceCode) => {

                if (faceCode === 1) {
                    return face1
                } else if (faceCode === 2) {
                    return face2
                } else if (faceCode === 3) {
                    return face3
                } else if (faceCode === 4) {
                    return face4
                } else {
                    return null
                }
            }

            return (
                <>
                    <DashboardItem>
                        <BtnRound
                            width={`120px`}
                            btncolor={displayColor(status)}>
                            <div style={{color: '#ffffff'}} className="Padding">{displayText(status)}</div>
                        </BtnRound>
                    </DashboardItem>
                    <DashboardItem style={{justifyContent: 'center'}}>
                        <Row>
                            <Tooltip title={<h3>Vital Signs</h3>}>
                            <span>
                                <BtnRound
                                    className="BtnText"
                                    width={`40px`}
                                    btncolor={BtnColor(vs)}>
                                <div style={{color: '#ffffff'}}>VS</div>
                                </BtnRound>
                            </span>
                            </Tooltip>
                            <Tooltip title={<h3>Sleep Status</h3>}>
                        <span>
                            <BtnRound
                                className="BtnText"
                                width={`40px`}
                                btncolor={BtnColor(sleep)}>
                                <div style={{color: '#ffffff'}}>SS</div>
                            </BtnRound>
                        </span>
                            </Tooltip>
                            <Tooltip title={<h3>IADL</h3>}>
                        <span>
                            <BtnRound
                                className="BtnText"
                                width={`40px`}
                                btncolor={BtnColor(IADL)}>
                            <div style={{color: '#ffffff'}}>IADL</div>
                        </BtnRound>
                        </span>
                            </Tooltip>
                        </Row>
                    </DashboardItem>
                    <DashboardItem style={{justifyContent: 'center'}}>
                    <span>
                        <img className="UserIc" src={FaceIc(UI)}/>
                    </span>
                    </DashboardItem>
                </>
            );
        }
        return (
            <>
                {this.state.dashData.map((item, index) => {
                    let {
                        name,
                        status,
                        vs,
                        sleep,
                        IADL,
                        UI
                    } = item;
                    return (
                        <DashboardCard style={{justifyContent: 'center'}}>
                            <Row height={'100%'} width={'100%'}>
                                <DashboardName>
                                    <div>{name}</div>
                                </DashboardName>
                                {dayStatus(status, vs, sleep, IADL, UI)}
                            </Row>
                        </DashboardCard>
                    )
                })}
            </>
        )
    }
}

const DashboardCard = styled(Paper)`
  width: 100%;
  height: 80px;
  margin-bottom: 20px;
  cursor: pointer;

  &&& {
    border-radius: 8px;
  }
`;

const DashboardName = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  justify-content: center;

  * {
    padding-left: 30px;
  }

  div {
    font-weight: bold;
    font-size: 1.1rem;
  }

  p {
    margin: 2px 0 0 0;
    font-size: 0.95rem;
    color: #222222;
  }

  @media screen and (max-width: 768px) {
    * {
      padding-left: 10px;
    }
  }
`;

const DashboardTitle = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  margin-top: 20px;

  div {
    flex: 1;
    font-weight: bold;
    font-size: 1.1rem;
  }
`;

const SiderButton = styled(ButtonBase)`
  width: 100%;
  height: 55px;

  &&& {
    color: #3b3b3b;
  }
`;

const DashboardItem = styled.div`
  flex: 1;
  width: auto;
  height: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  font-weight: bold;
  font-size: 1.1rem;

  span {
    font-weight: normal;
    font-size: 0.95rem;
    margin: 0 5px 0 5px;
  }

`;